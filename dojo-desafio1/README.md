# dojo-desafio1

> Desafio 1 Dojo Vuejs

## PRAZO
!!!!!
Segunda-feira 08h00

Lembrem de fazer push da brach

Dúvidas só pingar quem do contrato mexe com Vue.js, use o canal desenvolvimento do slack se precisar solicitando um help

## Objetivo

Seu serviço não está pronto, mas o contrato com ele já foi definido. E você irá fazer o front-end! BEM-VINDO ao grupo de Front-end devs!!!




Você receberá uma lista que contem unidades. Essa lista contém o seguinte formato:

``` json
{
  'recomendada': { // entidade Unidade
    'codigo': '<string> chave',
    'nome': '<string>',
    'endereco': '<string>',
    'cep': '<string>',
    'uf': '<string>',
    'detalhes': '<string>'
  },
  'unidades': [
      { // entidade Unidade
        'codigo': '<string> chave',
        'nome': '<string>',
        'endereco': '<string>',
        'cep': '<string>',
        'uf': '<string>',
        'detalhes': '<string>'
    },...
  ]
}
```

sento que a propriedade "recomendada" contém a unidade recomendada e a propriedade "unidades" é uma lista de objetos onde cada objeto é uma unidade.

Você vai criar uma tela acessivel em "\<host\>/#/unidades" ou "\<host\>/unidades"

Essa tela que terá uma "unidade recomendada" e embaixo uma lista de outras unidades vide exemplo:

![Prototipo 1](imagem1.png)

Cada item de unidade deve estar dentro de um componente então a caixa selecionada.

Ao clicar no card de uma unidade ela deverá ser selecionada.

Ao clicar no botão detalhes deve ser exibido um alerta com a propriedade 'detalhes' de uma unidade

![Prototipo 2 - outro item selecionado](imagem2.png)


## Dicas e refencias

1-) Faça um mock desses dados, um json de exemplo. Você pode coloca-lo no data ou criar um arquivo separado e fazer o import ou ainda declarar uma variavel na window que contenha os dados.

2-) Faça um componente que receba como uma propriedade um objeto Unidade

3-) O item selecionado pode ser feito via codigo da unidade e o componente só recebe uma "prop" marcando se está selecionado ou não
``` HTML
<!-- O codigo abaixo não é compilável e serve apenas como uma referencia -->
<elemento :propriedadebooleana="codigoSelecionado === codigo"> </elemento>
```

[Composing components](https://vuejs.org/v2/guide/components.html#Composing-Components)

[Passing data with props](https://vuejs.org/v2/guide/components.html#Props)

[Components and v-for](https://vuejs.org/v2/guide/list.html#Components-and-v-for)

[Git basics](https://www.atlassian.com/git/tutorials)



## Build Setup
Se seu computador não possui node js, instale-o:
(https://nodejs.org/en/download/)


``` bash
# Pega as ultimas alterações
git pull 

# Cria uma nova branch pra você
git checkout -b "<nome_da_sua_branch>"

# Envia para o servidor remote
git push -u origin "<nome_da_sua_brach>" ou apenas git push

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test



```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Regras do jogo: 

Pode fazer em grupo? Pode
Pode mandar codigo na master? Não, por favor, não :)
Tem problema não fazer? Não, é opcional, mas vai ser legal treinar os conhecimentos aprendidos até aqui e descobrir algumas coisas. Vou falar com seu chefe sobre isso (brincadeira, ok pessoal :)).